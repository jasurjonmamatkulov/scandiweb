function prodType(prod){
  var acmeAttributes = document.getElementById("acme_disc_attributes");
  var warPeaceAttributes = document.getElementById("war_peace_attributes");
  var chairAttributes = document.getElementById("chair_attributes");
  
  acmeAttributes.style.display="none";
  warPeaceAttributes.style.display="none";
  chairAttributes.style.display="none";
  
  if(prod=="Acme Disc"){
    acmeAttributes.style.display="block";
  }
  else if(prod=="War and Peace"){
    warPeaceAttributes.style.display="block";
  }
  else if(prod=="Chair"){
  chairAttributes.style.display="block";
  }
}